# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_20_185926) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "matches", force: :cascade do |t|
    t.datetime "date", default: -> { "CURRENT_DATE" }, null: false
    t.bigint "tournament_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["date"], name: "index_matches_on_date", using: :brin
    t.index ["tournament_id"], name: "index_matches_on_tournament_id"
  end

  create_table "participations", force: :cascade do |t|
    t.bigint "player_id", null: false
    t.bigint "tournament_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["player_id"], name: "index_participations_on_player_id"
    t.index ["tournament_id"], name: "index_participations_on_tournament_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "name", null: false
    t.string "city"
    t.string "email", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_players_on_email", unique: true
    t.index ["name"], name: "index_players_on_name"
  end

  create_table "statistics", force: :cascade do |t|
    t.boolean "won", null: false
    t.integer "hits", null: false
    t.integer "drunk", null: false
    t.bigint "player_id", null: false
    t.bigint "match_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id"], name: "index_statistics_on_match_id"
    t.index ["player_id"], name: "index_statistics_on_player_id"
  end

  create_table "tournaments", force: :cascade do |t|
    t.string "name", null: false
    t.string "location"
    t.datetime "date", default: -> { "CURRENT_DATE" }, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["date"], name: "index_tournaments_on_date", using: :brin
    t.index ["name"], name: "index_tournaments_on_name", unique: true
  end

  add_foreign_key "matches", "tournaments"
  add_foreign_key "participations", "players"
  add_foreign_key "participations", "tournaments"
  add_foreign_key "statistics", "matches"
  add_foreign_key "statistics", "players"
end
