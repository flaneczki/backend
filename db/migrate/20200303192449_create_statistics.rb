# frozen_string_literal: true

class CreateStatistics < ActiveRecord::Migration[6.0]
  def change
    create_table :statistics do |t|
      t.boolean :won, null: false
      t.integer :hits, null: false
      t.string :drunk, null: false

      t.references :player, index: true, null: false, foreign_key: true
      t.references :match, index: true, null: false, foreign_key: true
      t.timestamps
    end
  end
end
