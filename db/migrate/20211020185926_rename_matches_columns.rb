# frozen_string_literal: true

class RenameMatchesColumns < ActiveRecord::Migration[6.1]
  def change
    rename_column :matches, :match_date, :date
  end
end
