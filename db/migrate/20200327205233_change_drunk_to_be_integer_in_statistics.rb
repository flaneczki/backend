# frozen_string_literal: true

class ChangeDrunkToBeIntegerInStatistics < ActiveRecord::Migration[6.0]
  def up
    change_column :statistics, :drunk, "integer USING CAST(drunk AS integer)"
  end

  def down
    change_column :statistics, :drunk, "string USING CAST(drunk AS string)"
  end
end
