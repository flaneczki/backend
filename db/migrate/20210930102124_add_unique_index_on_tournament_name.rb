# frozen_string_literal: true

class AddUniqueIndexOnTournamentName < ActiveRecord::Migration[6.1]
  def change
    add_index :tournaments, :tournament_name, unique: true
  end
end
