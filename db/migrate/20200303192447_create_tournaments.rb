# frozen_string_literal: true

class CreateTournaments < ActiveRecord::Migration[6.0]
  def change
    create_table :tournaments do |t|
      t.string :tournament_name, null: false, unique: true
      t.string :location
      t.datetime :tournament_date, null: false, default: -> { "CURRENT_DATE" }

      t.timestamps
    end
    add_index :tournaments, :tournament_date, using: :brin
  end
end
