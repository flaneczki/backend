# frozen_string_literal: true

class CreateMatches < ActiveRecord::Migration[6.0]
  def change
    create_table :matches do |t|
      t.datetime :match_date, null: false, default: -> { "CURRENT_DATE" }
      t.references :tournament, index: true, null: false, foreign_key: true

      t.timestamps
    end
    add_index :matches, :match_date, using: :brin
  end
end
