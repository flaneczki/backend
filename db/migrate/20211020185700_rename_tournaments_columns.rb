# frozen_string_literal: true

class RenameTournamentsColumns < ActiveRecord::Migration[6.1]
  def change
    rename_column :tournaments, :tournament_name, :name
    rename_column :tournaments, :tournament_date, :date
  end
end
