# frozen_string_literal: true

FactoryBot.define do
  factory :tournament do
    sequence :name do |n|
      "Tournament#{n}"
    end
    location { "Sosnowiec" }
    date { "2020-03-03 23:07:57" }
  end
end
