# frozen_string_literal: true

FactoryBot.define do
  factory :statistic do
    won { true }
    hits { 6 }
    drunk { 9 }
    player
    match
  end
end
