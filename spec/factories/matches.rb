# frozen_string_literal: true

FactoryBot.define do
  factory :match do
    date { rand(1..100).days.from_now }
    tournament
  end
end
