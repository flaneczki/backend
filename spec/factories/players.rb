# frozen_string_literal: true

FactoryBot.define do
  factory :player do
    name { "Seba" }
    sequence :email do |n|
      "seba#{n}@email.pl"
    end
    city { "Sosnowiec" }
  end
end
