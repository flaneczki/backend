# frozen_string_literal: true

FactoryBot.define do
  factory :participation do
    association :player
    association :tournament
  end
end
