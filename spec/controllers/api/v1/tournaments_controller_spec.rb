# frozen_string_literal: true

require "rails_helper"

RSpec.describe Api::V1::TournamentsController, type: :controller do
  describe "GET api/v1/index" do
    before do
      FactoryBot.create_list(:tournament, 5)
      get :index, format: :json
    end

    it "returns a successful 200 response" do
      expect(response.status).to eq(200)
    end

    it "returns all the tournaments" do
      parsed_response = JSON.parse(response.body)
      expect(parsed_response.length).to eq(5)
    end
  end

  describe "GET api/v1/tournaments/:id" do
    let(:tournament) { create(:tournament) }

    it "returns a successful 200 response" do
      get :show, params: { id: tournament.id, format: :json }
      expect(response.status).to eq(200)
    end

    it "returns data of an single tournament statistics" do
      get :show, params: { id: tournament.id, format: :json }
      parsed_response = JSON.parse(response.body)
      expect(parsed_response).not_to be_nil
    end
  end

  describe "Update" do
    let(:tournament) { create(:tournament) }

    it "update tournament" do
      put :update, params: { id: tournament.id, tournament: { location: "Radom" } }, as: :json
      assert_response :success
    end

    it "doesnt update when parameters are invalid" do
      put :update, params: { id: tournament.id, tournament: { name: nil } }, as: :json
      assert_response :unprocessable_entity
    end
  end

  describe "Destroy" do
    let(:tournament) { create(:tournament) }

    it "destroy tournament" do
      delete :destroy, params: { id: tournament.id }, as: :json
      assert_response :no_content
    end
  end
end
