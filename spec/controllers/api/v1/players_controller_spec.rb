# frozen_string_literal: true

require "rails_helper"

RSpec.describe Api::V1::PlayersController, type: :controller do
  describe "GET api/v1/index" do
    before do
      FactoryBot.create_list(:player, 5)
      get :index, format: :json
    end

    it "returns a successful 200 response" do
      expect(response.status).to eq(200)
    end

    it "returns all the players" do
      parsed_response = JSON.parse(response.body)
      expect(parsed_response.length).to eq(5)
    end
  end

  describe "GET api/v1/players/:id" do
    let(:player) { create(:player) }

    it "returns a successful 200 response" do
      get :show, params: { id: player.id, format: :json }
      expect(response.status).to eq(200)
    end

    it "returns data of an single player statistics" do
      get :show, params: { id: player.id, format: :json }
      parsed_response = JSON.parse(response.body)
      expect(parsed_response).not_to be_nil
    end
  end

  describe "Update" do
    let(:player) { create(:player) }

    it "update player" do
      put :update, params: { id: player.id, player: { name: "Andrzej" } }, as: :json
      assert_response :success
    end

    it "doesnt update when parameters are invalid" do
      put :update, params: { id: player.id, player: { name: nil } }, as: :json
      assert_response :unprocessable_entity
    end
  end

  describe "Destroy" do
    let(:player) { create(:player) }

    it "destroy player" do
      delete :destroy, params: { id: player.id }, as: :json
      assert_response :no_content
    end
  end

  describe "Create" do
    context "with valid attributes" do
      it "creates new player" do
        player_params = FactoryBot.attributes_for(:player)
        expect { post :create, params: { player: player_params }, as: :json }.to change(Player, :count).by(1)
        assert_response :created
      end
    end

    context "with invalid attributes" do
      it "does not create new player" do
        expect { post :create, params: { player: { name: nil } }, as: :json }.not_to change(Player, :count)
        assert_response :unprocessable_entity
      end
    end
  end
end
