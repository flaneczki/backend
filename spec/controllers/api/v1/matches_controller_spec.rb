# frozen_string_literal: true

require "rails_helper"

RSpec.describe Api::V1::MatchesController, type: :controller do
  let!(:tournament) { FactoryBot.create(:tournament) }

  describe "GET api/v1/tournaments/:tournament_id/matches" do
    before do
      FactoryBot.create_list(:match, 5, tournament: tournament)
      get :index, params: { tournament_id: tournament.id }, format: :json
    end

    it "returns a successful 200 response" do
      expect(response.status).to eq(200)
    end

    it "returns all the matches" do
      parsed_response = JSON.parse(response.body)
      expect(parsed_response.length).to eq(5)
    end
  end

  describe "GET api/v1/tournaments/:tournament_id/matches/:id" do
    let(:match) { create(:match, tournament: tournament) }

    it "returns a successful 200 response" do
      get :show, params: { tournament_id: tournament.id, id: match.id, format: :json }
      expect(response.status).to eq(200)
    end

    it "returns data of an single match statistics" do
      get :show, params: { tournament_id: tournament.id, id: match.id, format: :json }
      parsed_response = JSON.parse(response.body)
      expect(parsed_response).not_to be_nil
    end
  end

  describe "Update" do
    let(:match) { create(:match, tournament: tournament) }

    it "update match" do
      put :update, params: { tournament_id: tournament.id, id: match.id, match: { date: Time.current } }, as: :json
      assert_response :success
    end

    it "doesnt update when parameters are invalid" do
      put :update, params: { tournament_id: tournament.id, id: match.id, match: { date: nil } }, as: :json
      assert_response :unprocessable_entity
    end
  end

  describe "Destroy" do
    let(:match) { create(:match, tournament: tournament) }

    it "destroy match" do
      delete :destroy, params: { tournament_id: tournament.id, id: match.id }, as: :json
      assert_response :no_content
    end
  end

  describe "Create" do
    context "with valid attributes" do
      it "creates new match" do
        tournament = FactoryBot.create(:tournament)
        match_params = FactoryBot.attributes_for(:match, tournament_id: tournament.id)
        expect { post :create, params: { tournament_id: tournament.id, match: match_params }, as: :json }.to change(Match, :count).by(1)
        assert_response :created
      end
    end

    context "with invalid attributes" do
      it "does not create new match" do
        expect { post :create, params: { tournament_id: tournament.id, match: { date: nil } }, as: :json }.not_to change(Match, :count)
        assert_response :unprocessable_entity
      end
    end
  end
end
