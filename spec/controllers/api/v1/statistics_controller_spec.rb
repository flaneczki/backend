# frozen_string_literal: true

require "rails_helper"

RSpec.describe Api::V1::StatisticsController, type: :controller do
  describe "GET api/v1/index" do
    before do
      FactoryBot.create_list(:statistic, 5)
      get :index, format: :json
    end

    it "returns a successful 200 response" do
      expect(response.status).to eq(200)
    end

    it "returns all the statistics" do
      parsed_response = JSON.parse(response.body)
      expect(parsed_response.length).to eq(5)
    end
  end

  describe "GET api/v1/statistics/:id" do
    let(:statistic) { create(:statistic) }

    it "returns a successful 200 response" do
      get :show, params: { id: statistic.id, format: :json }
      expect(response.status).to eq(200)
    end

    it "returns data of an single statistic" do
      get :show, params: { id: statistic.id, format: :json }
      parsed_response = JSON.parse(response.body)
      expect(parsed_response).not_to be_nil
    end
  end

  describe "Update" do
    let(:statistic) { create(:statistic) }

    it "update statistic" do
      put :update, params: { id: statistic.id, statistic: { hits: 3 } }, as: :json
      assert_response :success
    end

    it "doesnt update when parameters are invalid" do
      put :update, params: { id: statistic.id, statistic: { won: nil } }, as: :json
      assert_response :unprocessable_entity
    end
  end

  describe "Destroy" do
    let(:statistic) { create(:statistic) }

    it "destroy statistic" do
      delete :destroy, params: { id: statistic.id }, as: :json
      assert_response :no_content
    end
  end

  describe "Create" do
    context "with valid attributes" do
      it "creates new statistic" do
        player = FactoryBot.create(:player)
        match = FactoryBot.create(:match)
        statistic_params = FactoryBot.attributes_for(:statistic, match_id: match.id, player_id: player.id)
        expect { post :create, params: { statistic: statistic_params }, as: :json }.to change(Statistic, :count).by(1)
        assert_response :created
      end
    end

    context "with invalid attributes" do
      it "does not create new statistic" do
        expect { post :create, params: { statistic: { won: nil } }, as: :json }.not_to change(Statistic, :count)
        assert_response :unprocessable_entity
      end
    end
  end
end
