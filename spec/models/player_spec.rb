# frozen_string_literal: true

require "rails_helper"

RSpec.describe Player, type: :model do
  subject { FactoryBot.build(:player) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email) }
  it { is_expected.to have_many(:statistics) }
  it { is_expected.to have_many(:matches).through(:statistics) }
  it { is_expected.to have_many(:participations) }
  it { is_expected.to have_many(:tournaments).through(:participations) }
end
