# frozen_string_literal: true

require "rails_helper"

RSpec.describe Match, type: :model do
  subject { FactoryBot.build(:match) }

  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to have_many(:statistics) }
  it { is_expected.to have_many(:players).through(:statistics) }
  it { is_expected.to belong_to(:tournament) }
end
