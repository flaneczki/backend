# frozen_string_literal: true

require "rails_helper"

RSpec.describe Tournament, type: :model do
  subject { FactoryBot.build(:tournament) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to validate_uniqueness_of(:name) }
  it { is_expected.to have_many(:matches) }
  it { is_expected.to have_many(:participations) }
  it { is_expected.to have_many(:players).through(:participations) }
end
