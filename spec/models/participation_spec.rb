# frozen_string_literal: true

require "rails_helper"

RSpec.describe Participation, type: :model do
  subject { FactoryBot.build(:participation) }

  it { is_expected.to belong_to(:player) }
  it { is_expected.to belong_to(:tournament) }
end
