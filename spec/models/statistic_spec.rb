# frozen_string_literal: true

require "rails_helper"

RSpec.describe Statistic, type: :model do
  subject { FactoryBot.build(:statistic) }

  it { is_expected.to validate_presence_of(:won) }
  it { is_expected.to validate_presence_of(:hits) }
  it { is_expected.to validate_presence_of(:drunk) }
  it { is_expected.to belong_to(:player) }
  it { is_expected.to belong_to(:match) }
end
