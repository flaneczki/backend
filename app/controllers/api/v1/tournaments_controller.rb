# frozen_string_literal: true

module Api
  module V1
    class TournamentsController < ApplicationController
      expose(:tournaments) { Tournament.all }
      expose(:tournament)

      def index; end

      def show; end

      def create
        if tournament.save
          render json: tournament, status: :created
        else
          render json: tournament.errors, status: :unprocessable_entity
        end
      end

      def update
        if tournament.update(tournament_params)
          render json: tournament, status: :ok
        else
          render json: tournament.errors, status: :unprocessable_entity
        end
      end

      def destroy
        tournament.destroy
        head :no_content
      end

      private

      def tournament_params
        params.require(:tournament).permit(
          :name,
          :date,
          :location
        )
      end
    end
  end
end
