# frozen_string_literal: true

module Api
  module V1
    class MatchesController < ApplicationController
      expose(:matches) { Match.all }
      expose(:match)

      def index; end

      def show; end

      def update
        if match.update(params[:match])
          render json: match, status: :ok
        else
          render json: match.errors, status: :unprocessable_entity
        end
      end

      def destroy
        match.destroy
        head :no_content
      end

      def create
        if match.save
          render json: match, status: :created
        else
          render json: match.errors, status: :unprocessable_entity
        end
      end

      private

      def match_params
        params.require(:match).permit(
          :date,
          :tournament_id,
        )
      end
    end
  end
end
