# frozen_string_literal: true

module Api
  module V1
    class StatisticsController < ApplicationController
      expose(:statistics) { Statistic.all }
      expose(:statistic)

      def index; end

      def show; end

      def update
        if statistic.update(params[:statistic])
          render json: statistic, status: :ok
        else
          render json: statistic.errors, status: :unprocessable_entity
        end
      end

      def destroy
        statistic.destroy
        head :no_content
      end

      def create
        if statistic.save
          render json: statistic, status: :created
        else
          render json: statistic.errors, status: :unprocessable_entity
        end
      end

      private

      def statistic_params
        params.require(:statistic).permit(
          :won,
          :hits,
          :drunk,
          :player_id,
          :match_id
        )
      end
    end
  end
end
