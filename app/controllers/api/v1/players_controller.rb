# frozen_string_literal: true

module Api
  module V1
    class PlayersController < ApplicationController
      expose(:players) { Player.all }
      expose(:player)

      def index; end

      def show; end

      def update
        if player.update(player_params)
          render json: player, status: :ok
        else
          render json: player.errors, status: :unprocessable_entity
        end
      end

      def destroy
        player.destroy
        head :no_content
      end

      def create
        if player.save
          render json: player, status: :created
        else
          render json: player.errors, status: :unprocessable_entity
        end
      end

      private

      def player_params
        params.require(:player).permit(
          :name,
          :email,
          :city
        )
      end
    end
  end
end
