# frozen_string_literal: true

class Tournament < ApplicationRecord
  has_many :matches, dependent: :destroy

  has_many :participations, dependent: :destroy
  has_many :players, through: :participations

  validates :date, presence: true
  validates :name, presence: true, uniqueness: true
end
