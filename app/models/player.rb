# frozen_string_literal: true

class Player < ApplicationRecord
  has_many :statistics, dependent: :destroy
  has_many :matches, through: :statistics

  has_many :participations, dependent: :destroy
  has_many :tournaments, through: :participations

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
end
