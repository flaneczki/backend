# frozen_string_literal: true

class Match < ApplicationRecord
  belongs_to :tournament
  has_many :statistics, dependent: :destroy
  has_many :players, through: :statistics

  validates :date, presence: true
end
