# frozen_string_literal: true

class Statistic < ApplicationRecord
  belongs_to :player
  belongs_to :match

  validates :won, presence: true
  validates :hits, presence: true
  validates :drunk, presence: true
end
