# frozen_string_literal: true

Rails.application.routes.draw do
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :players, only: [:index, :show, :update, :destroy, :create]
      resources :tournaments, only: [:index, :show, :update, :destroy, :create] do
        resources :matches
      end
      resources :statistics, only: [:index, :show, :update, :destroy, :create]
    end
  end
end
