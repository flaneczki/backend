# frozen_string_literal: true

source "https://rubygems.org"

ruby "~> 3.0"

gem "rails", "~> 6.1"
gem "webpacker", "~> 5.0"
gem "turbolinks"
gem "puma"
gem "pg"
gem "decent_exposure"
gem "mini_racer"
gem "rack-cors", require: "rack/cors"
gem "pundit"
gem "sentry-ruby"
gem "sentry-rails"
gem "sentry-sidekiq"
gem "carrierwave"
gem "carrierwave-base64"
gem "bootsnap", ">= 1.4.2", require: false
gem "pagy"
gem "aasm"
gem "safely_block"
gem "lograge"
gem "jb"
gem "oj"
gem "dry-validation"

group :development do
  gem "mina"
  gem "letter_opener"
  gem "letter_opener_web"
  gem "database_consistency"
  gem "better_errors"
  gem "binding_of_caller"
  gem "overcommit"
  gem "fasterer"
  gem "rubocop", require: false
  gem "relaxed-rubocop"
  gem "rubocop-rails", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rspec", require: false
  gem "rails_best_practices"
  gem "rails-erd"
  gem "active_record_query_trace"
  gem "bundler-audit"
  gem "brakeman", require: false
  gem "traceroute"
  gem "factory_trace"
end

group :test do
  gem "simplecov", require: false
  gem "database_cleaner"
  gem "rspec_junit_formatter"
  gem "timecop"
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"
end

group :development, :test do
  gem "pry"
  gem "byebug"
  gem "pry-byebug"
  gem "pry-rails"
  gem "spring"
  gem "spring-watcher-listen"
  gem "rspec-rails"
  gem "shoulda-matchers"
  gem "listen"
  gem "bullet"
end

group :development, :test, :staging do
  gem "awesome_print"
  gem "factory_bot"
  gem "factory_bot_rails"
end
