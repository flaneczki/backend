# frozen_string_literal: true

begin
  require "awesome_print"
  AwesomePrint.pry!
rescue LoadError
  puts "Install awesome_print first!"
end
