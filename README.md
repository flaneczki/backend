# Flaneczki

## Database schema

![Database ERD diagram](./database_diagram.png)

## Refreshing ERD diagram

`EAGER_LOAD=1 bundle exec erd`
